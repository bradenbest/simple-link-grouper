const Checkbox = Object.seal({
    width:  15,
    height: 15,
    state:  0,
    parent: null,
    canvas: null,
    ctx:    null,
    extra_onclick: null,

    defaults: [{
        target:  document.body,
        label:   "",
        onclick: null,
        init:    0
    }],

    constructor: function({target, label, onclick, init}){
        this.parent = target;
        this.canvas = document.createElement("canvas");
        this.ctx = this.canvas.getContext('2d');
        this.extra_onclick = onclick;
        this.state = init;

        this.canvas.className = "canvas-checkbox";
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.canvas.addEventListener("click", this.ev_click());
        this.parent.appendChild(this.canvas);
        this.parent.appendChild(document.createTextNode(label));
        this.parent.appendChild(document.createElement("br"));
        this.draw();
    },

    ev_click: function(){
        let self = this;

        return function(unused){
            self.state = (self.state + 1) % 2;
            self.draw();

            if(self.extra_onclick)
                self.extra_onclick(self);
        }
    },

    draw_rect: function(color, offsetx, offsety){
        this.ctx.fillStyle = color;
        this.ctx.fillRect(offsetx, offsety,
                this.width - offsetx * 2, this.height - offsety * 2);
    },

    draw_state: function(){
        if(this.get_state())
            this.draw_rect("#000000", 2, 2);
    },

    draw: function(){
        this.draw_rect("#CCCCCC", 0, 0);
        this.draw_rect("#FFFFFF", 1, 1);
        this.draw_state();
    },

    get_state: function(){
        return +this.state;
    },

    set_state: function(state){
        this.state = +state;
        this.draw();
    }
});

const Checkbox3 = Checkbox.extend({
    ev_click: function(){
        let self = this;

        return function(unused){
            self.state = (self.state + 1) % 3;
            self.draw();

            if(self.extra_onclick)
                self.extra_onclick(self);
        }
    },

    draw_state: function(){
        switch(this.state){
            case 1:
                this.draw_rect("#000000", 2, 2);
                this.draw_rect("#FFFFFF", 5, 5);
                break;
            case 2:
                this.draw_rect("#000000", 2, 2);
                break;
        }
    }
});
