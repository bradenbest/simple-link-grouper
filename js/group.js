const Link = Object.seal({
    title:  null, /* string */
    url:    null, /* string */
    viewed: null, /* bool   */

    constructor: function(title, url){
        var viewed = url[0] === '-';
        var url_proper = url.substr(+viewed);

        this.title = title;
        this.url = url_proper;
        this.viewed = viewed;
    }
});

const Group = Object.seal({
    title: null, /* string */
    links: null, /* Link[] */
    prev:  null, /* Group  */
    next:  null, /* Group  */

    constructor: function(title, links){
        this.title = title;
        this.links = links;
    },

    to_string: function(){
        let self = this;
        let out = "";

        out += self.title + "\n";

        self.links.forEach(function(link, index){
            let is_final = index == self.links.length - 1 && self.next === null;

            out += ".title.viewed.url.endl"
                .replace(".title",  link.title ? (link.title + " | ") : "")
                .replace(".viewed", link.viewed ? "-" : "")
                .replace(".url",    link.url)
                .replace(".endl",   is_final ? "" : "\n");
        });

        if(self.next)
            out += "\n";

        return out;
    }
});

const GroupList = Object.seal({
    head:    null, /* Group */
    end:     null, /* Group */

    each: function(callback){
        for(let selnode = this.head; selnode !== null; selnode = selnode.next)
            callback(selnode);
    },

    add: function(group){
        if(Options.merge_groups.get_state())
            for(let selnode = this.head; selnode !== null; selnode = selnode.next)
                if(selnode.title === group.title)
                    return this.merge(selnode, group);

        return this.append(group);
    },

    remove: function(group){
        if(group === this.head)
            this.head = group.next;

        if(group === this.end)
            this.end = group.prev;

        if(group.next)
            group.next.prev = group.prev;

        if(group.prev)
            group.prev.next = group.next;

        group.prev = null;
        group.next = null;

        return group;
    },

    append: function(group){
        if(this.head === null)
            return (this.head = this.end = group);

        this.end.next = group;
        group.prev = this.end;
        this.end = group;

        return group;
    },

    merge: function(selnode, group){
        group.links.forEach(function(link){
            selnode.links.push(link);
        });

        return selnode;
    },

    destroy: function(){
        var temp;
        var selnode = this.head;

        while(selnode !== null){
            temp = selnode.next;
            selnode.prev = null;
            selnode.next = null;
            selnode = temp;
        }

        this.head = this.end = null;
    },

    is_empty: function(){
        return this.head === null;
    },

    to_string: function(){
        let out = "";

        this.each(function(group){
            out += group.to_string();
        });

        return out;
    }
});
