const Html = Object.seal({
    render: function(){
        var self = this;

        UI.container.innerHTML = "";

        GroupList.each(function(group){
            UI.container.appendChild(self.mk_group(group));
        });
    },

    mk_x_button: function(group){
        var el = document.createElement("span");
        var self = this;

        function ev_click(unused)
        {
            GroupList.remove(group);
            self.render();
        }

        el.className = "xbtn";
        el.innerHTML = "&#x00d7";
        el.addEventListener("click", ev_click);

        return el;
    },

    mk_group_title: function(title){
        var el = document.createElement("span");

        el.className = Options.same_line.get_state()
            ? "title"
            : "title-vertical";

        el.innerHTML = (title || "Untitled");

        return el;
    },

    mk_link: function(link, iterator){
        var el = document.createElement("a");
        var self = this;

        function ev_mouse(ev)
        {
            if(!link.viewed && Options.allow_marking.get_state()){
                el.className = "viewed-link";
                link.viewed = true;
            }

            if(Options.hide_marked.get_state())
                self.render();
        }

        if(link.viewed){
            if(Options.hide_marked.get_state())
                return null;

            el.className = "viewed-link";
        }

        else
            ["click", "auxclick"].forEach(function(ev_name){
                el.addEventListener(ev_name, ev_mouse);
            });

        el.target = "_blank";
        el.innerHTML = (link.title || iterator.toString());
        el.href = link.url;

        return el;
    },

    mk_link_container: function(){
        var el = document.createElement("div");

        el.className = Options.same_line.get_state()
            ? "link-container"
            : "link-container-vertical";

        return el;
    },

    mk_group: function(group){
        let self = this;
        let el = document.createElement("li");
        let link_container = self.mk_link_container();
        let iterator = 0;

        el.appendChild(self.mk_x_button(group));
        el.appendChild(self.mk_group_title(group.title));
        el.appendChild(link_container);

        group.links.forEach(function(link){
            let child = self.mk_link(link, ++iterator);

            if(!child)
                return;

            if(!Options.same_line.get_state())
                link_container.appendChild(document.createElement("br"));

            link_container.appendChild(child);
        });

        return el;
    }
});
