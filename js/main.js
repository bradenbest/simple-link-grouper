//const Debug =
(function(){
    const events = [
        [UI.render_btn,  "click",        ev_click_save_btn],
        [UI.render_btn,  "click",        ev_click_render_btn],
        [UI.dump_btn,    "click",        ev_click_dump_btn],
        [UI.dump_btn,    "click",        ev_click_save_btn],
        [UI.save_btn,    "click",        ev_click_save_btn],
        [UI.options_btn, "click",        ev_click_options_btn],
        [UI.help_btn,    "click",        ev_click_help_btn],
        [window,         "beforeunload", ev_beforeunload_window]
    ];

    function main()
    {
        events.forEach(function([target, type, callback]){
            target.addEventListener(type, callback);
        });

        Options.init();
        Storage.init();
        UI.init();
    }

    function ev_click_render_btn(unused)
    {
        if(!text_to_data())
            return;

        Html.render();
    }

    function ev_click_dump_btn(unused)
    {
        if(!data_to_text())
            return;
    }

    function ev_click_save_btn(unused)
    {
        if(Options.allow_storage.get_state()){
            data_to_text();
            Storage.write_state();
            Storage.write_disk();
        }

        else
            console.info("ev_click_save_btn(): Options.allow_storage (`Enable Storage`) is false. No action will be taken.");
    }

    function ev_click_help_btn(unused)
    {
        let visible = UI.help_text.style.display === "block";
        let frame_height = UI.help_text.style.height;

        UI.help_text.style.display = visible ? "none" : "block";

        UI.help_btn.value = ".showhide Help"
            .replace(".showhide", visible ? "Show" : "Hide");

        if(+frame_height.substr(0,1) === 0)
            UI.try_resize_iframe();
    }

    function ev_click_options_btn(unused)
    {
        var visible = UI.options_text.style.display === "block";

        UI.options_text.style.display = visible ? "none" : "block";

        UI.options_btn.value = ".showhide Options"
            .replace(".showhide", visible ? "Show" : "Hide");
    }

    function ev_beforeunload_window(ev)
    {
        let has_data = (UI.text.value != "" || !GroupList.is_empty());
        let allowed = Options.allow_storage.get_state();

        if((allowed && is_modified()) || !allowed && has_data)
            return ev.returnValue = "Are you sure? Unsaved changes will be lost.";
    }

    function link_from_line(line)
    {
        let contents = line.split(/ *\| */);

        if(contents.length === 1)
            return Link.create(null, contents[0]);
        else
            return Link.create(...contents);
    }

    function parse(text)
    {
        return text
            .split("\n\n")
            .map(group => group.split("\n"))
            .map(lines => Group.create(lines[0], lines.slice(1).map(link_from_line)));
    }

    function text_to_data()
    {
        if(UI.text.value === "")
            return false;

        parse(UI.text.value).forEach(function(group){
            GroupList.add(group);
        });

        UI.text.value = "";

        return true;
    }

    function data_to_text()
    {
        if(GroupList.is_empty())
            return false;

        text_to_data();

        UI.container.innerHTML = "";
        UI.text.value = GroupList.to_string();
        GroupList.destroy();

        return true;
    }

    function is_modified()
    {
        if(is_options_modified())
            return true;

        data_to_text();
        return Storage.state.text !== UI.text.value;
    }

    function is_options_modified()
    {
        let storage_opts = Storage.state.options;
        let internal_opts = Options.get_state();
        let keys = Object.keys(internal_opts);

        for(let i = 0, key; key = keys[i], i < keys.length; i++)
            if(internal_opts[key] !== storage_opts[key])
                return true;

        return false;
    }

    main();

    return { // Debug
        events,
        main,
        link_from_line,
        parse,
        text_to_data,
        data_to_text,
        is_modified,
        is_options_modified,
        ev_click_render_btn,
        ev_click_dump_btn,
        ev_click_save_btn,
        ev_click_options_btn,
        ev_click_help_btn,
        ev_beforeunload_window
    };
}());
