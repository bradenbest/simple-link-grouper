Object.prototype.create = function(...args){
    let retobj = Object.create(this);
    let true_args = args;

    if(args.length === 0)
        true_args = retobj.defaults || [null];
    else if(retobj.defaults)
        true_args = retobj.defaults.map(function(def, iterator){
            let arg = args[iterator];
            let out;

            if(typeof def !== "object")
                return arg || def;

            out = Object.keys(def).reduce(function(out, key){
                out[key] = arg[key] || def[key];
                return out;
            }, new Object);

            if(typeof out[0] === "undefined")
                return out;

            return Object.keys(out)
                .sort((a, b) => +a - +b)
                .filter(key => !isNaN(+key))
                .map(key => out[key]);
        });

    retobj.constructor(...true_args);

    return retobj;
}

Object.prototype.extend = function(newobj){
    var oldobj = Object.create(this);
    
    Object.keys(newobj).forEach(function(key){
        oldobj[key] = newobj[key];
    });

    return Object.seal(oldobj);
}

/* Example
 * 
 * const Class = {
 *     a: null,
 *     b: null,
 *     defaults: [0, 0],
 *     constructor: function(a, b){
 *         this.a = a;
 *         this.b = b;
 *     }
 * }
 *
 * myClass = Class.create(); // a = 0, b = 0
 * myClass = Class.create(1, 2); // a = 1, b = 2
 * can take varargs, objects, arrays, etc.
 */
