const Options = Object.seal({
    allow_marking: null,
    hide_marked: null,
    merge_groups: null,
    same_line: null,
    allow_storage: null,
    all: [
        "allow_marking",
        "hide_marked",
        "merge_groups",
        "same_line",
        "allow_storage",
    ],

    init: function(){
        this.allow_marking = Checkbox.create({
            target:  UI.options_text,
            label:   "Enable Link Marking",
            init:    true
        });

        this.hide_marked = Checkbox.create({
            target:  UI.options_text,
            label:   "Hide Marked Links",
            init:    false,
            onclick: function(unused){
                Html.render();
            }
        });

        this.merge_groups = Checkbox.create({
            target:  UI.options_text,
            label:   "Merge Groups with the same name",
            init:    true
        });

        this.same_line = Checkbox.create({
            target:  UI.options_text,
            label:   "Links on same line",
            init:    true,
            onclick: function(unused){
                Html.render();
            }
        });

        this.allow_storage = Checkbox3.create({
            target:  UI.options_text,
            label:   "Enable Storage",
            init:    0,
            onclick: function(self){
                let action = ([
                    "clear",
                    "promote_to_session",
                    "promote_to_local"
                ])[self.state];

                Storage[action]();
            }
        });
    },

    get_state: function(){
        let self = this;

        return self.all.reduce(function(out, name){
            out[name] = self[name].get_state();
            return out;
        }, {});
    }
});
