const Storage = Object.seal({
    id: "Link Grouper",
    state: null,
    device: null,

    init: function(){
        this.device = localStorage;

        if(!this.device.getItem(this.id))
            this.device = sessionStorage;

        this.read_disk();
        this.read_state();
    },

    write_state: function(){
        let self = this;

        this.state.text = UI.text.value;
        this.state.options = {};

        Options.all.forEach(function(node){
            self.state.options[node] = Options[node].get_state();
        });
    },

    read_state: function(){
        let self = this;

        UI.text.value = this.state.text;

        Options.all.forEach(function(node){
            Options[node].set_state(self.state.options[node]);
        });
    },

    write_disk: function(){
        this.device.setItem(this.id, JSON.stringify(this.state));
    },

    read_disk: function(){
        let data;

        this.state = {};
        this.write_state();

        if(data = this.device.getItem(this.id))
            this.state = JSON.parse(data);
    },

    clear: function(){
        this.device.removeItem(this.id);
        this.read_disk();
        UI.save_btn.disabled = true;
    },

    promote_to_session: function(){
        this.device = sessionStorage;
        UI.save_btn.disabled = false;
    },

    promote_to_local: function(){
        this.device = localStorage;
        this.device.setItem(this.id, sessionStorage.getItem(this.id));
        sessionStorage.removeItem(this.id);
    }
});
