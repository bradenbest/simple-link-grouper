const UI = Object.freeze({
    text          : document.querySelector("textarea"),
    render_btn    : document.querySelector(".button.render"),
    dump_btn      : document.querySelector(".button.dump"),
    save_btn      : document.querySelector(".button.save"),
    options_btn   : document.querySelector(".button.options"),
    options_text  : document.querySelector("div.options"),
    help_btn      : document.querySelector(".button.help"),
    help_text     : document.querySelector("iframe.help"),
    container     : document.querySelector(".links"),

    init: function(){
        let self = this;

        self.save_btn.disabled = !Options.allow_storage.get_state();

        if(document.domain === "")
            console.info("This app appears to be running locally. The iframe's sizing may be a little wonky.");
    },
    
    try_resize_iframe: function(){
        let self = this;
        let help = self.help_text;

        if(document.domain === "" || help.contentDocument.body === null)
            return false;

        help.style.height = help.contentDocument.body.scrollHeight + "px";

        return true;
    }
});
