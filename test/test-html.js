const dummy_op = state => ({get_state: ()=>state});

const Options = {
    allow_marking: dummy_op(false),
    hide_marked:  dummy_op(false),
    merge_groups: dummy_op(false),
    same_line: dummy_op(true)
};

const UI = {
    container: display
};

var testgroup = {
    title: "Test group",
    links: [
        {url: "http://example.com", title: "Link 1", viewed: false},
        {url: "http://example.com", title: "Link 2", viewed: true},
        {url: "http://example.com", title: "Link 3", viewed: false},
    ],
    prev: null,
    next: null
};

function do_run([hide_marked, same_line], display){
    Options.hide_marked = dummy_op(hide_marked);
    Options.same_line = dummy_op(same_line);
    UI.container = display;
    Html.render();
}

GroupList.add(testgroup);

do_run([false, true],  display);
do_run([true,  true],  display2);
do_run([false, false], display3);
do_run([true,  false], display4);
