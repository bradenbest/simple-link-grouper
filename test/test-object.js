const Tester = Object.seal({
    a: null,
    b: null,

    sum: function(){
        return this.a + this.b;
    }
});

const Tester1 = Tester.extend({
    defaults: [{
        a: 0,
        b: 0
    }],

    constructor: function({a, b}){
        this.a = a;
        this.b = b;
    }
});

const Tester2 = Tester.extend({
    defaults: [[0, 0]],

    constructor: function([a, b]){
        this.a = a;
        this.b = b;
    }
});

const Tester3 = Tester.extend({
    defaults: [0, 0],

    constructor: function(a, b){
        this.a = a;
        this.b = b;
    }
});

const head_template = "Type     Arguments       a   b   Result   Expected/Actual";

const label_templates = [
    "Object   .<hl().>             .a   .b   .res    .exp/.act   ",
    "Object   .<hl({}).>           .a   .b   .res    .exp/.act   ",
    "Object   .<hl({a:1}).>        .a   .b   .res    .exp/.act   ",
    "Object   .<hl({a:1, b:2}).>   .a   .b   .res    .exp/.act   ",
    "Object   .<hl({b:1, a:2}).>   .a   .b   .res    .exp/.act   ",
    "Array    .<hl().>             .a   .b   .res    .exp/.act   ",
    "Array    .<hl([]).>           .a   .b   .res    .exp/.act   ",
    "Array    .<hl([1]).>          .a   .b   .res    .exp/.act   ",
    "Array    .<hl([1, 2]).>       .a   .b   .res    .exp/.act   ",
    "Varargs  .<hl().>             .a   .b   .res    .exp/.act   ",
    "Varargs  .<hl(1).>            .a   .b   .res    .exp/.act   ",
    "Varargs  .<hl(1, 2).>         .a   .b   .res    .exp/.act   "
];

function gen_result(result){
    const text = ["fail", "pass"];

    return "<span class=.class>.text</span>"
        .replace(".class", text[+result])
        .replace(".text",  text[+result].toUpperCase());
}

let actual_obj = [
    Tester1.create(),
    Tester1.create({}),
    Tester1.create({a: 1}),
    Tester1.create({a: 1, b: 2}),
    Tester1.create({b: 1, a: 2}),

    Tester2.create(),
    Tester2.create([]),
    Tester2.create([1]),
    Tester2.create([1, 2]),

    Tester3.create(),
    Tester3.create(1),
    Tester3.create(1, 2)
];

let actual = actual_obj.map(obj => obj.sum());

let expect = [
    0, 0, 1, 3, 3,
    0, 0, 1, 3,
    0, 1, 3
];

let results = actual.map((actual, i) => expect[i] === actual);

document.body.appendChild(function(){
    let el = document.createElement("p");

    el.className = "header";
    el.innerHTML = head_template.replace(/ /g, "&nbsp;");

    return el;
}());

label_templates.forEach(function(label, iterator){
    document.body.appendChild(function(){
        let el = document.createElement("p");

        el.innerHTML = label
            .replace(/ /g, "&nbsp;")
            .replace(".<hl", "<span class=highlight>")
            .replace(".>", "</span>")
            .replace(".a", actual_obj[iterator].a)
            .replace(".b", actual_obj[iterator].b)
            .replace(".res", gen_result(results[iterator]))
            .replace(".exp", expect[iterator])
            .replace(".act", actual[iterator]);

        return el;
    }());
});

document.body.appendChild(function(){
    let el = document.createElement("p");

    el.innerHTML = ".passed of .total tests passed"
        .replace(".passed", results.reduce((a, b)=>(a += +b), 0))
        .replace(".total", results.length);

    return el;
}());
